#!/usr/bin/env bash

DOTFILES="$(cd "$(dirname "${BASH_SOURCE[0]}")/.." && pwd -P)"
source "$DOTFILES/lib/common.sh"

bot "asdf is an extendable version manager with support"
bot "for ruby, node, elixir and more."

# asdf is NOT installed
if ! brew ls --versions asdf > /dev/null; then
    read -r -p "install asdf? [y|N] " response
    if [[ $response =~ ^(y|yes|Y) ]]; then
        brew install asdf
    fi
    ok
fi

# asdf is installed
if brew ls --versions asdf > /dev/null; then
    bot "Now we'll install some asdf plugins and tools."
    bot "To customize what gets installed, modify:"
    item "$DOTFILES/asdf/plugins"
    item "$DOTFILES/asdf/tool-versions.symlink"
    read -r -p "install asdf plugins and tools? [y|N] " response
    if [[ $response =~ ^(y|yes|Y) ]]; then
        INSTALLED="$(asdf plugin-list)"
        PLUGINS="$(dirname "$0")/plugins"

        if [ -f ${PLUGINS} ]; then
            while read LINE
            do
                case "$LINE" in \#*) continue ;; esac
                IFS=' ' read -a plugin <<< "$LINE"
                if [ ${plugin[0]} ]; then
                if ! grep -q ${plugin[0]} <<< "$INSTALLED" ; then
                    if [ ${plugin[1]} ]; then
                        installing "plugin ${plugin[0]} from ${plugin[1]}"
                        asdf plugin-add ${plugin[0]} ${plugin[1]}
                    else
                        installing "plugin ${plugin[0]}"
                        asdf plugin-add ${plugin[0]}
                    fi

                    # Import NodeJs OpenPGP keys to main keyring
                    if [ "${plugin[0]}" = 'nodejs' ]; then
                        installing "OpenPGP keys for nodejs"
                        bash /usr/local/opt/asdf/plugins/nodejs/bin/import-release-team-keyring
                    fi
                else
                    skip "plugin ${plugin[0]}"
                fi
            fi
            done < ${PLUGINS}

            updating "asdf plugins"
            asdf plugin-update --all

            installing "asdf tool versions"
            asdf install
        else
            error "Could not find ${PLUGINS}"
        fi
    fi
    ok
fi