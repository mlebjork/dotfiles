#!/usr/bin/env bash

DOTFILES="$(cd "$(dirname "${BASH_SOURCE[0]}")/.." && pwd -P)"
source "$DOTFILES/lib/common.sh"

# Fix permissions for zsh-completions
if [ -f /usr/local/share ]; then
    chmod go-w /usr/local/share
fi

# Install zplug
if [ ! -f ~/.zplug/init.zsh ]; then
    installing "zplug"
    curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh| zsh
fi
