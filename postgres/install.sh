#!/usr/bin/env bash

DOTFILES="$(cd "$(dirname "${BASH_SOURCE[0]}")/.." && pwd -P)"
source "$DOTFILES/lib/common.sh"

# Create default postgres user
if test $(which brew); then
    if brew ls --versions postgresql > /dev/null; then
        roles="$(psql postgres -t -c '\du')"
        if ! grep -q " postgres " <<< "$roles" ; then
            psql postgres -c "CREATE ROLE postgres WITH LOGIN PASSWORD 'postgres' SUPERUSER;"
        fi
    fi
fi