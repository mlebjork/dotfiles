#!/usr/bin/env bash

DOTFILES="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd -P)"
source "$DOTFILES/lib/common.sh"

# Prevent sleep while install is running
caffeinate &

#####
# Introduction
#####

print_header

full_name=$(osascript -e "long user name of (system info)")

bot "Hi $full_name. I'm going to setup your OSX system. We're going to:"
item "Install Xcode command line tools"
item "Setup your gitconfig"
item "Link dotfiles in your home directory"
item "Install the Homebrew package manager"
item "Install all your brews and casks"
item "Set some more sensible OS X defaults"
item "Set ZSH as your default shell"
item "Run any custom install scripts"
item "Install Neovim and vim plugins"

bot "One more thing: I'll need your password from time to time."

read -r -p "Let's roll? [y|N] " response
if [[ $response =~ ^(y|yes|Y) ]];then
    ok
else
    exit -1
fi

#####
# Install Xcode command line tools
#####

checking "Xcode CLI installed"
xcode_select="xcode-select --print-path"
xcode_install=$($xcode_select) 2>&1 > /dev/null
if [[ $? != 0 ]]; then
    info "You are missing the Xcode CLI tools. I'll launch the install for you, but then you'll have to restart the process again."
    running "After that you'll need to paste the command and press Enter again."

    read -r -p "Let's go? [y|N] " response
    if [[ $response =~ ^(y|yes|Y) ]];then
        xcode-select --install
    fi

    exit -1
fi
ok

#####
# Configure git
#####

checking "gitconfig"
if ! [ -f "$DOTFILES/git/gitconfig.local.symlink" ]; then
    running "configuring git"
    credential="osxkeychain"

    prompt "Your name for git commit messages? [$full_name]"
    read -e author
    prompt "Your email for git commit messages?"
    read -e email
    prompt "Your ssh username? [$LOGNAME]"
    read -e ssh_user
    prompt "Your GitHub username?"
    read -e github_user

    author=${author:-"$full_name"}
    ssh_user=${ssh_user:-"$LOGNAME"}
    sed -e "s/AUTHOR_NAME/$author/g" -e "s/AUTHOR_EMAIL/$email/g" -e "s/SSH_USER/$ssh_user/g" -e "s/GITHUB_USER/$github_user/g" -e "s/GIT_CREDENTIAL_HELPER/$credential/g" "$DOTFILES/git/gitconfig.local.symlink.example" > "$DOTFILES/git/gitconfig.local.symlink"
fi
ok

#####
# Symlink dotfiles
#####

bot "Now we'll symlink your dotfiles into you home directory."
note "Do this on the first install or whenever any new symlinks are added."
note "This will symlink the following dotfiles:"
symlink_dotfiles -d "$DOTFILES"

read -r -p "symlink your dotfiles? [y|N] " response
if [[ $response =~ ^(y|yes|Y) ]]; then
    symlink_dotfiles "$DOTFILES"
#   for src in $symlinks; do
#     # TODO: look at replacing nested symlinks with
#     # https://github.com/holman/dotfiles/issues/101#issuecomment-50263838
#     # relative_src=$(echo $src | sed -e "s=$DOTFILES==" -e "s=.symlink==")
#     # dst="$HOME/.$(echo $relative_src | cut -d "/" -f3-)"
#     dst=$(basename "${src%.*}")
#     dst="$HOME/.${dst//+//}"
#     link_file "$src" "$dst"
#   done
fi
ok

#####
# Install homebrew
#####

checking "homebrew installation"
brew_bin=$(which brew) 2>&1 > /dev/null
if [[ $? != 0 ]]; then
    running "installing homebrew"
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    if [[ $? != 0 ]]; then
        error "unable to install homebrew, script $0 abort!"
        exit -1
    fi
else
    running "updating homebrew"
    # brew update
fi
ok

#####
# Homebrew bundle
#####

bot "Now we're ready to install all your homebrew packages!"
note "To customize which brews and casks get installed,"
note "quit now and modify $DOTFILES/Brewfile"
option "y" "install packages"
option "u" "install and upgrade packages"
option "N" "skip (continue without installing)"
option "q" "quit and edit Brewfile"
read -r -p "Install homebrew packages? [y|N|u|q] " response
if [[ $response =~ ^(y|yes|Y) ]]; then
    running "brew bundle --no-upgrade"
    brew bundle --no-upgrade --file="$DOTFILES/Brewfile"
elif [[ $response =~ ^(u|up|U|UP) ]]; then
    running "brew bundle"
    brew bundle --file="$DOTFILES/Brewfile"
elif [[ $response =~ ^(q|quit|Q) ]]; then
    bot "Okay, run install.sh again when you're ready."
    exit 0
fi
ok

#####
# OS X defaults
#####

bot "Now we'll set some sensible OS X defaults"
read -r -p "are you ready? [y|N] " response
if [[ $response =~ ^(y|yes|Y) ]]; then
    running "setting OS X defaults"
    sh "$DOTFILES/macos/set-defaults.sh"
fi
ok

#####
# Code directory
#####

if [ ! -d ~/Code ]; then
    info "We'll assume all your source code lives in the ~/Code directory"
    note "This is just a convenience for zsh aliases and such."
    read -r -p "create this directory now? [y|N] " response
    if [[ $response =~ ^(y|yes|Y) ]]; then
        running "creating code directory [~/Code]"
        mkdir -p ~/Code
    fi
    ok
fi

#####
# ZSH
#####

if ! test "$(echo $SHELL)" = "/usr/local/bin/zsh"; then
    bot "Now we'll set your default shell to ZSH"
    read -r -p "are you ready? [y|N] " response
    if [[ $response =~ ^(y|yes|Y) ]]; then
        # Add homebrew zsh to /etc/shells if it does not exist.
        if ! grep -Fxq "/usr/local/bin/zsh" /etc/shells; then
            running "updating /etc/shells"
            echo "updating /etc/shells"
            echo "/usr/local/bin/zsh" | sudo tee -a /etc/shells >/dev/null
        fi

        # Set default shell to zsh.
        running "changing shell to zsh"
        chsh -s /usr/local/bin/zsh
    fi
    ok "You'll need to restart your terminal for ZSH to take effect"
fi

#####
# Run topic install scripts
#####

bot "Now we'll run all remaining topic install.sh scripts"
bot "This will run:"
installers=$(find "$DOTFILES" -depth 2 -name install.sh)
for script in $installers; do
    item "${script}"
done
read -r -p "run scripts? [y|N] " response
if [[ $response =~ ^(y|yes|Y) ]]; then
    for script in $installers; do
        running "${script}"
        sh -c "${script}"
    done
fi
ok

#####
# Run topic install scripts
#####

bot "Finally, we'll install Neovim and a bunch of handy vim plugins."
read -r -p "continue? [y|N] " response
if [[ $response =~ ^(y|yes|Y) ]]; then
    sh "$DOTFILES/vim/install-nvim.sh"
fi
ok

#####
# Finish Up
#####

echo ""
bot "Congratulations! We're all done installing everyting."
bot "You'll need to restart your terminal in order for"
bot "all these changes to take effect."

if test $(which hyper); then
    if [[ "$TERM_PROGRAM" != "Hyper" ]]; then
        bot "I noticed you have Hyper terminal installed. Good choice!"
        read -r -p "Shall I open a new Hyper terminal? [y|N] " response
        if [[ $response =~ ^(y|yes|Y) ]]; then
            hyper &
        fi
    fi
fi