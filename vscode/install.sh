#!/usr/bin/env bash

DOTFILES="$(cd "$(dirname "${BASH_SOURCE[0]}")/.." && pwd -P)"
source "$DOTFILES/lib/common.sh"

code -v > /dev/null
if [[ $? -eq 0 ]]; then
    bot "Now we'll install some Visual Studio extensions and custom settings"

    read -r -p "install extensions? [y|N] " response
    if [[ $response =~ ^(y|yes|Y) ]]; then
        EXTENSIONS="$(dirname "$0")/extensions"

        if [ -f ${EXTENSIONS} ]; then
            while read LINE
            do
                case "$LINE" in \#*) continue ;; esac
                IFS=' ' read -a extension <<< "$LINE"
                if [ ${extension[0]} ]; then
                    installing "extension ${extension[0]}"
                    code --install-extension "${extension[0]}"
                fi
            done < ${EXTENSIONS}
        fi
    fi
    ok

    installing "Visual Studio Code settings.json"
    src="$DOTFILES/vscode/settings.json"
    target="$HOME/Library/Application Support/Code/User/settings.json"
    symlink_file "$src" "$target"
    ok
fi
