#!/usr/bin/env bash

DOTFILES="$(cd "$(dirname "${BASH_SOURCE[0]}")/.." && pwd -P)"
source "$DOTFILES/lib/common.sh"

if test $(which nvim); then
    if [[ ! $(pip3 show pynvim) ]]; then
        echo "Installing Python pynvim client"
        pip3 install pynvim
    else
        echo "Upgrading Python pynvim client"
        pip3 install --upgrade pynvim
    fi

    # if [[ `gem list -i neovim` = 'false' ]]; then
    #   echo "Installing Ruby neovim client"
    #    gem install neovim
    # else
    #   echo "Updating Ruby neovim client"
    #   gem update neovim
    # fi
fi

# Check for vim-plug
if [ ! -f ~/.local/share/nvim/site/autoload/plug.vim ]; then
    echo "Installing vim-plug"
    curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi

echo "Installing vim plugins"
nvim +PlugUpgrade +PlugInstall +PlugUpdate +PlugClean! +qa
