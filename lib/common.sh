#!/usr/bin/env bash

DOTFILES="$(cd "$(dirname "${BASH_SOURCE[0]}")/.." && pwd -P)"

# Colors
ESC_SEQ="\x1b["
COL_RESET=$ESC_SEQ"39;49;00m"
COL_GREY=$ESC_SEQ"30;01m"
COL_RED=$ESC_SEQ"31;01m"
COL_GREEN=$ESC_SEQ"32;01m"
COL_YELLOW=$ESC_SEQ"33;01m"
COL_BLUE=$ESC_SEQ"34;01m"

function bot() {
    echo -e "$COL_GREEN(◕‿◕)$COL_RESET $1"
}

function info() {
    echo -e "$COL_BLUE[info]$COL_RESET $1"
}

function checking() {
    echo -e "$COL_GREY[checking]$COL_RESET $1"
}

function status() {
    echo -e "$COL_GREY[...]$COL_RESET $1"
}

function installing() {
    echo -e "$COL_YELLOW[installing]$COL_RESET $1..."
}

function updating() {
    echo -e "$COL_YELLOW[updating]$COL_RESET $1..."
}

function running() {
    echo -e "$COL_YELLOW[running]$COL_RESET $1..."
}

function action() {
    echo -e "$COL_YELLOW[action]$COL_RESET\n ⇒ $1..."
}

function note() {
    echo -e "$COL_GREY[note]$COL_RESET $1"
}

function option() {
    echo -e "  $COL_YELLOW[$COL_RESET$1$COL_YELLOW]$COL_RESET $2"
}

function item() {
    echo -e "  $COL_YELLOW[*]$COL_RESET $1"
}

function prompt() {
    echo -e "$COL_BLUE[?]$COL_RESET $1"
}

function dry() {
    echo -en "$COL_GREY[dry-run]$COL_RESET"
}

function ok() {
    echo -e "$COL_GREEN[ok]$COL_RESET $1"
}

function warn() {
    echo -e "$COL_YELLOW[warning]$COL_RESET $1"
}

function error() {
    echo -e "$COL_RED[error]$COL_RESET $1"
}

function backup() {
    echo -e "$COL_GREEN[backup]$COL_RESET $1"
}

function symlink() {
    echo -e "$COL_BLUE[symlink]$COL_RESET $1 $COL_GREY=>$COL_RESET $2"
}

function create() {
    echo -e "$COL_GREEN[mkdir]$COL_RESET $1"
}

function copy() {
    echo -e "$COL_RED[copy]$COL_RESET $1"
}

function remove() {
    echo -e "$COL_RED[remove]$COL_RESET $1"
}

function skip() {
    if [ -n "$1" ]; then
        echo -e "$COL_GREY[skip]$COL_RESET $1"
    else
        echo -en "$COL_GREY[skip]$COL_RESET"
    fi
}

function print_header() {
    echo -en "\n$COL_BLUE          ██            ██     ████ ██  ██ $COL_RESET"
    echo -en "\n$COL_BLUE         ░██           ░██    ░██░ ░░  ░██ $COL_RESET"
    echo -en "\n$COL_BLUE         ░██  ██████  ██████ ██████ ██ ░██  █████   ██████ $COL_RESET"
    echo -en "\n$COL_BLUE      ██████ ██░░░░██░░░██░ ░░░██░ ░██ ░██ ██░░░██ ██░░░░ $COL_RESET"
    echo -en "\n$COL_BLUE     ██░░░██░██   ░██  ░██    ░██  ░██ ░██░███████░░█████ $COL_RESET"
    echo -en "\n$COL_BLUE    ░██  ░██░██   ░██  ░██    ░██  ░██ ░██░██░░░░  ░░░░░██ $COL_RESET"
    echo -en "\n$COL_BLUE    ░░██████░░██████   ░░██   ░██  ░██ ███░░██████ ██████ $COL_RESET"
    echo -en "\n$COL_BLUE     ░░░░░░  ░░░░░░     ░░    ░░   ░░ ░░░  ░░░░░░ ░░░░░░ $COL_RESET"
    echo -en "\n"
    echo -en "\n"
    echo -en "\n"
}

function symlink_file() {
    show_help() { echo "USAGE: symlink_file: [-h | -d | -o | -b | -s]" 1>&2; exit; }
    local OPTIND
    local dry_run=false overwrite_all=false backup_all=false skip_all=false

    while getopts "h?dobs:" opt; do
        case "$opt" in
            h|\?)
            show_help
            exit 0
            ;;
            d)  dry_run=true
            ;;
            o)  overwrite_all=true
            ;;
            b)  backup_all=true
            ;;
            s)  skip_all=true
            ;;
        esac
    done
    shift $((OPTIND-1))
    # [ "${1:-}" = "--" ] && shift

    local src=$1 target=$2
    local overwrite= backup= skip=
    local action=

    if [ -f "$target" -o -d "$target" -o -L "$target" ]; then
        if [ "$overwrite_all" != "true" ] && [ "$backup_all" != "true" ] && [ "$skip_all" != "true" ]; then
            local currentSrc="$(readlink "$target")"

            if [ "$currentSrc" == "$src" ]; then
            skip=true
            else
            prompt "File already exists: $target ($(basename "$src")), what do you want to do?\n\
            [s]kip, [S]kip all, [o]verwrite, [O]verwrite all, [b]ackup, [B]ackup all?"
            read -r action

            case "$action" in
                o )
                overwrite=true;;
                O )
                overwrite_all=true;;
                b )
                backup=true;;
                B )
                backup_all=true;;
                s )
                skip=true;;
                S )
                skip_all=true;;
                * )
                ;;
            esac
            fi
        fi

        overwrite=${overwrite:-$overwrite_all}
        backup=${backup:-$backup_all}
        skip=${skip:-$skip_all}

        if [ "$overwrite" == "true" ]; then
            if [ "$dry_run" == "true" ]; then
                dry
            else
                rm -rf "$target"
            fi
            remove "$target"
        fi

        if [ "$backup" == "true" ]; then
            if [ "$dry_run" == "true" ]; then
                dry
            else
                mv "$target" "${target}.backup"
            fi
            backup "$target"
        fi

        if [ "$skip" == "true" ]; then
            if [ "$dry_run" == "true" ]; then
                dry
            fi
            skip; symlink "$src" "$target"
        fi
    fi

    if [ "$skip" != "true" ]; then
        parent=$(dirname "$2")
        if [ ! -d "$parent" ]; then
            if [ "$dry_run" == "true" ]; then
                dry
            else
                mkdir -p "$parent"
            fi
            create "$parent"
        fi
        if [ "$dry_run" == "true" ]; then
            dry
        else
            ln -s "$src" "$target"
        fi
        symlink "$src" "$target"
    fi
}

function symlink_dotfiles() {
    show_help() { echo "USAGE: symlink_dotfiles: [-h | -d | -m]" 1>&2; exit; }
    local OPTIND
    local dry_run=false maxdepth=2

    while getopts "h?dm:" opt; do
        case "$opt" in
        h|\?)
            show_help
            exit 0
            ;;
        d)  dry_run=true
            ;;
        m)  maxdepth=$OPTARG
            ;;
        esac
    done
    shift $((OPTIND-1))
    # [ "${1:-}" = "--" ] && shift

    local sources=$(find -H "$DOTFILES" -maxdepth $maxdepth -name '*.symlink' -not -path '*.git*')

    for src in $sources; do
        target=$(basename "${src%.*}")
        target="$HOME/.${target//+//}"
        if [ "$dry_run" == "true" ]; then
            dry; symlink "$src" "$target"
        else
            symlink_file "$src" "$target"
        fi
    done
}
