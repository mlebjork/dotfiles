# Pipe my public key to my clipboard.
alias pubkey="pbcopy < ~/.ssh/id_rsa.pub; echo 'id_rsa.pub copied to clipboard'"
