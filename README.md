# sporkd's dotfiles

Heavily customized fork of holman's dotfiles.
http://zachholman.com/2010/08/dotfiles-are-meant-to-be-forked/

## Quick Install (not forked)

If you want all my configs and don't want to customize much,
then you can just clone and install:

```sh
git clone https://gitlab.com/sporkd/dotfiles.git ~/.dotfiles
cd ~/.dotfiles
./install.sh
```

The install script will walk you through symlinking the the appropriate
files in `.dotfiles` to your home directory.

Now simply pull down the latest and run `install.sh` from time to time to keep
your environment fresh and up-to-date:

```sh
git pull
./install.sh
```

## Forked Install (recommended)

Otherwise, if you'd like to customize your own dotfiles but
still merge in updates I'm working on... you'll want to
[fork sporkd/dotfiles](https://gitlab.com/sporkd/dotfiles/fork),
and track my changes as an upstream branch:

```sh
git clone https://gitlab.com/(your-username)/dotfiles.git ~/.dotfiles
cd ~/.dotfiles
(make custom changes)
./install.sh
```

The install script will walk you through symlinking the the appropriate
files in `.dotfiles` to your home directory.

Don't forget to push any custom changes you've made up to your fork.

```sh
(more custom changes)
git commit
git push origin master
```

Now from time to time, you can merge any upstream changes back into your fork,
and run `install.sh` to keep your environment fresh and up-to-date:


```sh
git remote add upstream git@gitlab.com:sporkd/dotfiles.git
git fetch upstream
git merge upstream/master
git push origin master
./install.sh
```

## Topic Areas

Everything's built around topic areas. A topic can be anything with configs
you'd like to track in your dotfiles repo. So for example, shell configs,
programming languages, editors and development tools. To get a better idea,
have a look at some of existing topic directories that already exist,
(e.g. `zsh`, `git`, `vim`, `vscode`). But you can add more!

Say for example, the new hot editor you recently installed (the NewHotEditor),
creates a directory in your home called `~/.newhotedit`, with configs you
want to start tracking. In that case, you'd simply move that directory inside
your `~/.dotfiles` directory so that it can be symlinked and tracked in git.

```sh
mv ~/.newhotedit ~/.dotfiles/newhotedit.symlink
cd ~/.dotfiles
./install.sh
git add newhotedit.symlink
git commit -m "Tracking new hot editor configs"
```

The `.symlink` extension on a directory or file tells the install script to
symlink that directory up into your home directory (but without the `.symlink`
extension). That way the rcfile is still where the the editor expects to
find it (~/.rcfile), but it can now be tracked by git in your dotfiles repo
(`~/.dotfiles/rcfile.symlink`).

In addition to `.symlink`, here a few other special files and directories
in the repo, and what they do:

- **bin/**: Anything in `bin/` will get added to your `$PATH` and be made
  available everywhere.
- **topic/path.zsh**: Any file named `path.zsh` is loaded first and is expected
  to setup `$PATH` or similar.
- **topic/\*.zsh**: Any files ending in `.zsh` will get loaded into your zsh
  environment. Use these to set topic specific zsh configs
  (e.g. `zsh/aliases.zsh`).
- **topic/completion.zsh**: Any file named `completion.zsh` is loaded last
  and is expected to setup autocomplete.
- **topic/install.sh**: Any file named `install.sh` in a sub folder, is for
  installing topic specific dependencies, and gets executed when you run the
  top level `~/.install.sh`.
- **topic/\*.symlink**: Files or directories ending in `*.symlink` get symlinked
  into your `$HOME`. To further nest symlinks into subdirectories under `$HOME`,
  use `+` signs to signify additional directory delimiters. So for example, the
  file `topic/config+topic.symlink` would get symlinked to `$HOME/.config/topic`
  when you run the install script.

## Additional Customizations

Here are some additional files you might want to customize before running
`install.sh`:

- **[Brewfile](https://gitlab.com/sporkd/dotfiles/blob/master/Brewfile)**:
  Use [Hombrew Bundle](https://github.com/Homebrew/homebrew-bundle)
  to customize all the brews and brew casks that you commonly like to install
  on your Mac.
- **[asdf/plugins](https://gitlab.com/sporkd/dotfiles/blob/master/asdf/plugins)** and
  **[asdf/tool-versions.symlink](https://gitlab.com/sporkd/dotfiles/blob/master/asdf/tool-versions.symlink)**:
  The [asdf-vm](https://asdf-vm.com/) is an extendable version manager (Think
  Node Version Manager, but with pluggable support for other languages like
  Ruby, Elixir, Erlang and more). Customize these two files to determine what
  pluins and tools get installed, and ditch the nvm, rvm, rbenv alphabet
  soup!
- **[git/gitconfig.symlink](https://gitlab.com/sporkd/dotfiles/blob/master/git/gitconfig.symlink)**:
  Configures git aliases and other things. Specifically, you might want to set
  the default editor that opens on `git commit` (which defaults to nvim).
- **[hyper/hyper.js.symlink](https://gitlab.com/sporkd/dotfiles/blob/master/hyper/hyper.js.symlink)**:
  The [Hyper Terminal](https://hyper.is/) is a super nice Electron-based
  terminal. Don't worry, you can still use iTerm or Terminal, but Hyper gets
  installed by default in the Brewfile, and many settings in this repo are
  optimized for Hyper use. Give it a try!
- **[vscode/extensions](https://gitlab.com/sporkd/dotfiles/blob/master/vscode/extensions)**:
  This file configures all the extensions that will be installed for
  [Visual Studio Code](https://code.visualstudio.com/).
  ***WARNING!!!*** I'm using `vscodevim.vim`, so you probably want to comment
  out that line if you don't like using vim mode in vscode!
- **[zsh/zplug](https://gitlab.com/sporkd/dotfiles/blob/master/zsh/zplug)**:
  [zplug](https://github.com/zplug/zplug) is a next-generation plugin manager
  for zsh (think oh-my-zsh improved). Modify this file to configure which
  zsh plugins get installed by default.
- **[zsh/zsh.symlink](https://gitlab.com/sporkd/dotfiles/blob/master/zsh/zshrc.symlink)**:
  By default, a project directory (`~/Code`) will be created for you to
  store your project source code. This allows for some useful shortcuts.
  For example: `c<enter>` will quickly cd into `~/Code`, and `c my-proj<tab>`
  will auto-complete to `~/Code/my-project-name`. You can change the default
  location by modifying `export PROJECTS=~/Code`.
